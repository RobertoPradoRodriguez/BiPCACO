#!/bin/bash
#SBATCH -n 4
#SBATCH --ntasks-per-node=4
#SBATCH --mem=16GB
#SBATCH -c 1
#SBATCH -t 00:10:00

module load cesga/2020 intel impi
module load jdk/17.0.2

export HOME=/home/ulc/es/rpr/BiPCACO
export NUM_PROC_MPI=4
export OMP_NUM_THREADS=1
export _JAVA_OPTIONS=""

srun $HOME/build/aco

#!/bin/bash
#SBATCH -n 1
#SBATCH --ntasks-per-node=1
#SBATCH --mem=10GB
#SBATCH -c 1
#SBATCH -t 00:05:00

module load cesga/2020 intel impi
module load jdk/17.0.2

export HOME=/home/ulc/es/rpr/BiPCACO
export NUM_PROC_MPI=1
export OMP_NUM_THREADS=1
export _JAVA_OPTIONS=""

srun $HOME/build/aco

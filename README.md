# BiPCACO

## Purpose
This repo is hosting the development of the C++-based BiPCACO algorithm.


## Content
* `/aco` C++ code
  * `aco.hpp` header file.
  * `ants.cpp` file containing the ant-related function (e.g. deposit/evaporate pheromone).
  * `bipcaco.cpp` file with BiPCACO algorithm functions.
  * `individuals.cpp` file to define the individuals of the population.
  * `parallel.cpp` parallel functions to communication among processes.
  * `utilities.cpp` helper functions (e.g. print and input/output functions).

* `/build` Hold the executable file and Makefile created using "CMake".

* `CMakeLists.txt` for building C++ code.

* `/JAVA_W-model` Hold the jar file with the cost function. 

* `/SLURM-examples` Hold examples of script to launch a sequential and a parallel job in SLURM job scheduler.


## Setup

To download this repository execute the following command:

git clone https://gitlab.com/RobertoPradoRodriguez/bipcaco.git

* There are three prerequisites (preferably latest versions): CMake, Intel MPI (or other MPI library) and Java JDK.


## How to run

0. Edit CMakeLists file.

    0.1. Update CMAKE_CXX_FLAGS with paths to JDK: 
    
    set(CMAKE_CXX_FLAGS "-I [/path/to/header-files] -L [/path/to/shared-libraries]")
    
    * Paths containing jni_md.h (typically ../jdk/_JDK-version_/include/linux) and libjvm.so (typically ../jdk/_JDK-version_/lib/server) respectively.                                                   ````````` 
    
    0.2 Update path to shared library `libjvm`:

    target_link_libraries(aco PRIVATE aco_lib [/path/to/shared/library/libjvm.so] ${OpenMP_CXX_FLAGS} ${MPI_C_LIBRARIES})

    * The same path used in 0.1 (../jdk/_JDK-version_/lib/server/libjvm.so).


1. Compilation process. 

   Once in the main folder (BiPCACO), execute:

    * rm -rf build/ && mkdir build  --> Remove previous config.
    * cd build/ && cmake ..         --> Generate new Makefile.
    * make                          --> Compile & link. Executable `aco` is created in build folder.

2. Execution/Testing.

    There is a `parameter.txt` file to specify the values of the variables that should be on the same folder where you execute the program.
    If it is not provided, default parameters will be used.   
    
    There are two ways to run the algorithm:
    
    * Using SLURM job scheduler.
      
     `/SLURM-examples` contains two sub-folders to launch a sequential and a parallel job to test the application. 
     Each sub-folder has a predefined `parameters.txt`.
     
     To launch a job:   $ sbatch _job.sh_

    * Using `mpirun` (example for Intel MPI).
    
    $ mpirun -n <number-of-processes> -ppn <processes-per-node> -f <hostfile> ./aco
   
    -f specifies the path to the host file listing the cluster nodes; 
    alternatively, you can use the -hosts option to specify a comma-separated list of nodes; 
    if hosts are not specified, the local node is used. 
    (https://www.intel.com/content/www/us/en/docs/mpi-library/developer-guide-linux/2021-6/running-an-mpi-program.html)


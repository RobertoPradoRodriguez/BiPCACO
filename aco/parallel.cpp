/**
 * @file parallel.c
 * @authors patricia.gonzalez@udc.es / roberto.prado@udc.es
 * @brief File containing general functions about the parallelization of the 
 * algorithm with MPI.
 */

#include <signal.h>
#include <assert.h>
#include <limits.h>
#include <time.h>
#include "aco.hpp"

#ifdef MPI2

MPI_Request request[200][200];
MPI_Request req_send[100][100];
MPI_Request SRrequest[6];
MPI_Status status;

int mpi_id;
int NPROC;

int **comm_solution;
int *best_solution_to_send;

void init_parallel ( void )
{
 
	MPI_Init(NULL,NULL);
 
	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_id);
	MPI_Comm_size(MPI_COMM_WORLD, &NPROC);

}

/**
 Routine to start recv. of communications from Colonies
 **/

void startComm ( void )
{
	int i;

	// Allocation of the communication buffers
	if ((comm_solution = (int **) malloc(sizeof(int *)*NPROC))==NULL) {
		printf("Comm Solution. Out of memory, exit.");
		exit(1);
	}
	for ( i = 0 ; i < NPROC ; i++ )
		comm_solution[i] = (int *) malloc(sizeof(int)*(n+1));
	
	if ((best_solution_to_send = (int *) malloc(sizeof(int)*(n+1)))==NULL) {
		printf("Best solution to send. Out of memory, exit.");
		exit(1);
	}
	
	// Prepare to receive the best solutions found in the rest of the colonies
	for (i=0 ; i<NPROC ; i++)
		if (i!=mpi_id)
		MPI_Irecv(&comm_solution[i][0], n+1, MPI_INT, i,
				  3000, MPI_COMM_WORLD, &request[i][ntry]);
	
}

/**
 Routine to send the best solution found to the rest of Colonies
 **/
void sendBest ( void )
{
	
	for ( int i = 0; i < n; i++)
		best_solution_to_send[i] = best_so_far_individual->solution[i];
	best_solution_to_send[n] = best_so_far_individual->score;

	// Send best solution found to the rest of the colonies
	for( int i = 0 ; i < NPROC ; i++ )
		if ( i != mpi_id ){
			// Free req_send objects
			if (iteration > 1)
				MPI_Request_free(&req_send[i][ntry]);
			MPI_Isend(best_solution_to_send, n+1, MPI_INT, i,
				3000, MPI_COMM_WORLD, &req_send[i][ntry]);
		}
}

void listen_aco(JNIEnv* env)
{
	int         i, j;
	int         listen;
	
	// Loop to listen best solutions from colonies
	for( i = 0; i < NPROC ; i++) {
		
		if( i != mpi_id ){
		  
			listen = 1;          
			while ( listen == 1 ) {
				MPI_Test(&request[i][ntry], &listen, &status);
				
				if ( listen ) {
					if (comm_solution[status.MPI_SOURCE][n] <= optimal)
						best_so_far_individual->score = comm_solution[status.MPI_SOURCE][n];

					else if ((mpi_id+1) % cfreq == 0 && comm_solution[status.MPI_SOURCE][n] < best_so_far_individual->score && stall_iters >= cstall){
						
						for ( j = 0 ; j < n ; j++ )
							best_so_far_individual->solution[j] = comm_solution[status.MPI_SOURCE][j];
						best_so_far_individual->score = comm_solution[status.MPI_SOURCE][n];

						trail_max = 1. / ( (rho) * best_so_far_individual->score );
						trail_min = trail_max / ( 2. * n );
						trail_0 = trail_max;
					 
						if (mpi_id == 0)
							if ( report ) fprintf(report,"%d \t %d \t %.3f\n",best_so_far_individual->score, evals, elapsed_time(REAL));
					}

					// Prepare to receive more solutions
					MPI_Irecv(&comm_solution[status.MPI_SOURCE][0], n+1, MPI_INT,
							  status.MPI_SOURCE, 3000, MPI_COMM_WORLD, &request[i][ntry]);
				}
			}
		}
	}
}


/**
 Routine to clear up pending messages in the asynchronous version
 **/
void exit_parallel ( void )
{
	int i, listen, flag;
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	// Reception of lost pending messages
	for( i = 0 ; i < NPROC ; i++ ) {
		if( i != mpi_id ){
			listen = 1;
			while ( listen ) {
				MPI_Test(&request[i][ntry], &listen, &status);
				if ( listen ){
					MPI_Irecv(&comm_solution[status.MPI_SOURCE][0], n+1, MPI_INT,
								status.MPI_SOURCE, 3000, MPI_COMM_WORLD, &request[i][ntry]);
				}
			}
			MPI_Cancel(&request[i][ntry]);
		}
	}
	
	// Free req_send objects
	for(int i = 0 ; i < NPROC ; i++)
		if ( i != mpi_id )
			MPI_Request_free(&req_send[i][ntry]);
	
	for ( i = 0 ; i < NPROC ; i++ ) 
		free( comm_solution[i] );

	free(comm_solution);
	free(best_solution_to_send);
}
	   

/***************************  Final MPI-Report ************************************/

void init_parallel_report( void )
/*
 FUNCTION:       prepare report file,
 INPUT:          none
 OUTPUT:         none
 COMMENTS:
 */
{
	char temp_buffer[255];

	if (mpi_id == 0 ) { 
		sprintf(temp_buffer,"conv_report.%dx%d",NPROC,omp_get_num_threads());
		report = fopen(temp_buffer, "w");
		sprintf(temp_buffer,"final_report.%dx%d",NPROC,omp_get_num_threads());
		final_report = fopen(temp_buffer, "w");
		fprintf(final_report,"Try:\t Best:\t bIter:\t bTime:\t\t Tot.Time:\t evals:\t\t nrestarts:\t nproc:\t \n");
		sprintf(temp_buffer,"results_report.%dx%d",NPROC,omp_get_num_threads());
		results_report = fopen(temp_buffer, "w");
	}

}

/**
 Routine to write one summarize mpi report
 **/
void write_parallel_report ( void )
{
	if( mpi_id == 0 ){
		// To receive from processes
		int p_evals, p_iter, p_restarts, p_score;
		int best_process = mpi_id;
		double p_time;

		int *p_solution;
        if ((p_solution = (int *) malloc(sizeof(int)*(n)))==NULL) {
            printf("P_solution failed. Out of memory, exit.");
            exit(1);
        }

		for(int i=1 ; i<NPROC ; i++ ) {
			MPI_Recv(p_solution, n, MPI_INT, i, 1111, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			MPI_Recv(&p_score, 1, MPI_INT, i, 2222, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			MPI_Recv(&p_iter, 1, MPI_INT, i, 3333, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			MPI_Recv(&p_time, 1, MPI_DOUBLE, i, 5555, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			MPI_Recv(&p_restarts, 1, MPI_INT, i, 6666, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			MPI_Recv(&p_evals, 1, MPI_INT, i, 4444, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	
			if ((p_score == best_ever_individual->score && p_time < best_time && p_time > 0.00) 
			   || p_score < best_ever_individual->score) {
				
				best_iteration = p_iter;
				best_time = p_time;
				best_process = i;
				n_restarts = p_restarts;
				evals = p_evals;
				
				for (int j = 0; j < n; j++)
                    best_ever_individual->solution[j] = p_solution[j];
                best_ever_individual->score = p_score;
			} 
		}
		free(p_solution);

		if (final_report)
			fprintf(final_report, "%d\t %d\t %d\t %.3f\t\t %.3f\t\t %d\t\t %d\t\t %d\n",
				ntry, best_ever_individual->score, best_iteration, best_time, elapsed_time( REAL ),
				evals, n_restarts, best_process);
		fprintSolutions();
		
		fflush(final_report);
		fflush(report);
		fflush(results_report);
	
	}
	else{
		MPI_Isend(best_ever_individual->solution, n, MPI_INT, 0, 1111, MPI_COMM_WORLD,&SRrequest[0]);
		MPI_Isend(&best_ever_individual->score, 1, MPI_INT, 0, 2222, MPI_COMM_WORLD,&SRrequest[1]);
		MPI_Isend(&best_iteration, 1, MPI_INT, 0, 3333, MPI_COMM_WORLD,&SRrequest[2]);
		MPI_Isend(&evals, 1, MPI_INT, 0, 4444, MPI_COMM_WORLD,&SRrequest[3]);
		MPI_Isend(&best_time, 1, MPI_DOUBLE, 0, 5555, MPI_COMM_WORLD,&SRrequest[4]);
		MPI_Isend(&n_restarts, 1, MPI_INT, 0, 6666, MPI_COMM_WORLD,&SRrequest[5]);
	}
}
#endif

/**
 * @file aco.hpp
 * @authors patricia.gonzalez@udc.es / roberto.prado@udc.es
 * @brief Header file for the program.
 */

#include <vector>
#include <jni.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

// Comment this define appropriately
#define MPI2
#define OMP

#ifdef OMP
   #include <omp.h>
#endif

#ifdef MPI2
   #include <mpi.h>
#endif

/**************** RANDOM NUMBERS *********************/
#define __MTWISTER_H

#define STATE_VECTOR_LENGTH 624
#define STATE_VECTOR_M      397 /* changes to STATE_VECTOR_LENGTH also require changes to this */

typedef struct tagMTRand {
	unsigned long mt[STATE_VECTOR_LENGTH];
	int index;
} MTRand;

MTRand seedRand(unsigned long seed);
unsigned long genRandLong(MTRand* rand);
double genRand(MTRand* rand);

extern MTRand seedR;

#define HEURISTIC(m,n)     (1.0 / ((double) 0.1))
/* add a small constant to avoid division by zero if a distance is 
zero */

typedef struct {
	int *solution;
	int score;
} individual_struct;

extern individual_struct *individual;           // All population
extern individual_struct *best_so_far_individual;
extern individual_struct *best_ever_individual;

extern int n;          // Problem size
extern int n_pop;      // Number of individuals

// Ant related
extern double **pheromone;		// pheromone matrix, two entries for each gate
extern double rho;           	// parameter for evaporation
//extern double alpha, beta;	// importance of trail and heuristic evaluate

extern int as_flag;     		// run ant system
extern int eas_flag;    		// run elitist ant system
extern int mmas_flag;   		// run MAX-MIN ant system

extern double trail_max;     	// maximum pheromone trail in MMAS
extern double trail_min;      // minimum pheromone trail in MMAS
extern double trail_0;        // initial pheromone trail level
extern int u_gb;              // every u_gb iterations update with best-so-far ant

// Limits
extern int max_tries;
extern int max_evals;
extern int max_iters;
extern double max_time;
extern int restart_iters;
extern int stall_iters;

extern int ntry;
extern int evals;
extern int iteration;
extern int best_iteration;
extern int restart_best;
extern int n_restarts;
extern double best_time;
extern int optimal;				// optimal solution value or bound to find

// Cooperation parameters
#ifdef MPI2
extern int cfreq;
extern int cstall;
extern int auto_coop;
#endif

// W-Model params
extern int _n;
extern int _mu;
extern int _nu;
extern int _gamma;

/*****************     Parallel   ***********************/
#ifdef MPI2
extern MPI_Request request[200][200];
extern MPI_Request req_send[100][100];
extern MPI_Request SRrequest[6];
extern MPI_Status status;

extern int mpi_id;
extern int NPROC;

extern int **comm_solution;		// solutions from other colonies
extern int *best_solution_to_send;
#endif

extern FILE *report, *final_report, *results_report;

/***************************** TIMER **************************************/
typedef enum type_timer {REAL, VIRTUAL} TIMER_TYPE;

/***************************** UTILITIES **************************************/
#define INFTY                 LONG_MAX

#define TRUE  1
#define FALSE 0

/* general macros */

#define MAX(x,y) ((x)>=(y)?(x):(y))
#define MIN(x,y) ((x)<=(y)?(x):(y))

extern long int seed;

int ** generate_int_matrix( int n, int m);

double ** generate_double_matrix( int n, int m);

/********************************   Parallel ******************************/
#ifdef MPI2
void init_parallel(void);

void exit_parallel(void);

void sendBest(void);

void startComm(void);

void listen_aco(JNIEnv* env);

void write_parallel_report(void);

void init_parallel_report(void);
#endif

/***************************** ACO **************************************/
void construct_solutions ( void );

void pheromone_trail_update ( void );

void as_update ( void );

void eas_update ( void );

void mmas_update ( void );

void check_pheromone_trail_limits( void );

void init_pheromone_trails ( double initial_trail );

void evaporation ( void );

void global_update_pheromone ( individual_struct *a );

void global_update_pheromone_weighted ( individual_struct *a, int weight );

void select_gate( individual_struct *a, int phase );

/***********************************************************************/
int termination_condition ( void );

void init_optimization ( void );

void exit_optimization ( void );

void update_statistics ( void );

/* Auxiliary procedures related to individuals */

int find_best ( void );

void copy_from_to(individual_struct *a1, individual_struct *a2);

void allocate_individuals( void );

/***************************** IN-OUT **************************************/
void set_default_parameters();

void read_parameters();

void init_report();

void write_report();

void print_parameters ( void );

void fprintSolutions ( void );

/***************************** TIMER **************************************/
void start_timers(void);

double elapsed_time(TIMER_TYPE type);

/**
 * @file bipcaco.cpp
 * @authors patricia.gonzalez@udc.es / roberto.prado@udc.es
 * @brief File contains main procedures
 */

#include "aco.hpp"
#include <climits>

using namespace std;

int ntry;
#ifdef MPI2
int min_cstall, max_cstall;
#endif

jclass myclass;
jmethodID mymethod;

JNIEnv* create_vm() {
	JavaVM *jvm;
	JNIEnv *env;
	JavaVMInitArgs vm_args;
	JavaVMOption *options = new JavaVMOption[1];

	options[0].optionString = (char *)"-Djava.class.path=/home/ulc/es/rpr/BiPCACO/JAVA_W-model/func3.jar";
	vm_args.version = JNI_VERSION_1_6;
	vm_args.nOptions = 1;
	vm_args.options = options;
	vm_args.ignoreUnrecognized = false;

	// load and initialize a Java VM, return a JNI interface pointer in env
	jint res = JNI_CreateJavaVM(&jvm, (void**)&env, &vm_args);
	if (res != JNI_OK)
		printf("Failed to create Java VM.\n");
	delete options;

	myclass = env->FindClass("cn/edu/hfuu/iao/WModel/Main2");
	if(myclass == nullptr) 
		printf("ERROR: class not found!\n");

	mymethod = env->GetStaticMethodID(myclass, "function", "(IIII[I)I");

	return env;
}

/*************************    ACO procedures  *****************************/

void construct_solutions (JNIEnv* env)
/*
 FUNCTION:       manage the solution construction phase
 INPUT:          none
 OUTPUT:         none
 (SIDE)EFFECTS:  when finished, all ants of the colony have constructed a solution
 */
{   
	jintArray q = env->NewIntArray(n);
	jint tmp[n];
	
	for ( int k = 0 ; k < n_pop ; k++ ) {
		for (int step = 0; step < n; step++)
			select_gate(&individual[k], step);
		//Evaluation
		for (int i = 0; i < n; i++) tmp[i] = individual[k].solution[i];
		env->SetIntArrayRegion(q, 0, n, tmp);
		individual[k].score = env->CallStaticIntMethod(myclass, mymethod, _n, _mu, _nu, _gamma, q);
		evals++;
		
	}
	env->DeleteLocalRef(q);
}

/*************************    common procedures  *****************************/

int termination_condition( void )
/*    
	  FUNCTION:       checks whether termination condition is met 
	  INPUT:          none
	  OUTPUT:         0 if condition is not met, number neq 0 otherwise
	  (SIDE)EFFECTS:  none
*/
{
	// Check limits
	if (evals >= max_evals || iteration >= max_iters || elapsed_time(REAL) >= max_time)
		return 1;
	
	// Check solution
	return best_so_far_individual->score <= optimal;
}


void compute_scores(JNIEnv* env)
/*
 FUNCTION:       compute scores
 INPUT:          none
 OUTPUT:         none
 */
{   
	jintArray q = env->NewIntArray(n);
	jint tmp[n];

	for ( int k = 0 ; k < n_pop ; k++ ) {
		//Evaluation
		for (int i = 0; i < n; i++) tmp[i] = individual[k].solution[i];
		env->SetIntArrayRegion(q, 0, n, tmp);
		individual[k].score = env->CallStaticIntMethod(myclass, mymethod, _n, _mu, _nu, _gamma, q);
	}
	env->DeleteLocalRef(q);
	evals = evals + n_pop;
}

void init_individuals (JNIEnv* env)
/*
 FUNCTION:       initialization of population
 INPUT:          none
 OUTPUT:         none
 (SIDE)EFFECTS:  when finished, all individuals are initialized
 */
{
	int rnd;
	jintArray q = env->NewIntArray(n);
	jint tmp[n];
	
	for (int k = 0; k < n_pop; k++) {
		for (int j = 0; j < n; j++) {
			rnd = round (genRand( &seedR )); // random number 0 or 1
			individual[k].solution[j] = rnd;
		}

		for (int i = 0; i < n; i++) tmp[i] = individual[k].solution[i];
		env->SetIntArrayRegion(q, 0, n, tmp);
		individual[k].score = env->CallStaticIntMethod(myclass, mymethod, _n, _mu, _nu, _gamma, q);
	}
	env->DeleteLocalRef(q);
	evals = evals + n_pop;
}

void init_optimization( void )
/*    
	  FUNCTION: initilialize variables appropriately when starting a trial
	  INPUT:    none
	  OUTPUT:   none
	  COMMENTS: none
*/
{
	// W-Model problem size
	n = _n * _mu;

#ifdef MPI2    
	if (auto_coop){
		min_cstall = n/NPROC;

		if (min_cstall > 200)
			max_cstall = 0.1 * restart_iters;
		else if (min_cstall > 150)
			max_cstall = 0.2 * restart_iters;
		else if (min_cstall > 100)
			max_cstall = 0.3 * restart_iters;
		else if (min_cstall > 50)
			max_cstall = 0.4 * restart_iters;
		else
			max_cstall = 0.5 * restart_iters;
		
		min_cstall = 0;
		cstall = max_cstall;
	}
#endif

	// Allocate individuals
	allocate_individuals();

	// Initialize variables concerning statistics etc.
	evals = 0;
	iteration = 1;
	best_iteration = 1;
	best_so_far_individual->score = best_ever_individual->score = n;
   
	start_timers();
	best_time = 0.0;
	stall_iters = 0;
  
	printf("*********** Start Try: %d *****************\n",ntry); 
	if (report){ 
		fprintf(report,"\n******** Try: %d **********\n",ntry);
		fprintf(report,"Score\t Evals\t Time\n");
		fprintf(report,"-----\t -----\t ----\n");
	}    
	//if (report_cc) fprintf(report_cc,"\n",ntry);
   
	// Allocate communication buffers
#ifdef MPI2
	startComm();
#endif
	restart_best = 1;
	n_restarts = 0;

	// allocate pheromone matrix
	pheromone = generate_double_matrix( n, 2 );
	// Initialize pheromone trails
	trail_max = 1. / ( (rho) * 0.5 );
	trail_min = trail_max / ( 2. * n );
	trail_0 = trail_max;
	init_pheromone_trails( trail_0 );
}

void exit_optimization( void )
/*
 FUNCTION: end trial
 INPUT:    none
 OUTPUT:   none
 COMMENTS: none
 */
{   
#ifdef MPI2    
	printf("\t*********** exit proc: %d *****************\n", mpi_id);
#endif
	printf("\tSTOP TRY %d iteration %d, elapsed_time %.3f, score %d, stall_iters %d\n", ntry, iteration, elapsed_time( REAL ), best_so_far_individual->score, stall_iters);
	printf("\t*******************************************\n\n");

#ifdef MPI2
	write_parallel_report();
	exit_parallel();
#else
	write_report();
#endif
   
	for (int i = 0 ; i < n_pop ; i++)
		free(individual[i].solution);
	free(individual);

	free(best_so_far_individual->solution);
	free(best_so_far_individual);

	free(best_ever_individual->solution);
	free(best_ever_individual);

	free(pheromone);
}
	
void update_statistics( JNIEnv* env )
/*    
	  FUNCTION:       manage some statistical information
					  if a new best solution is found
	  INPUT:          none
	  OUTPUT:         none
*/
{

	int iteration_best = find_best();
 
	if ( individual[iteration_best].score < best_so_far_individual->score ) {
		
		copy_from_to( &individual[iteration_best], best_so_far_individual ); 
		stall_iters = 0;
        restart_best = iteration;

		if (best_so_far_individual->score < best_ever_individual->score){
			copy_from_to(best_so_far_individual, best_ever_individual);
			best_time = elapsed_time( REAL ); 
			best_iteration = iteration;
		}
		
#ifdef MPI2
		if (NPROC > 1)
			sendBest (); // Send best to the rest of the Colonies   
#endif        

		// Update trails limits
        trail_max = 1. / ( (rho) * best_so_far_individual->score );
		trail_min = trail_max / ( 2. * n );
		trail_0 = trail_max;
		
#ifdef MPI2  
		if (mpi_id == 0)
#endif
		if ( report ) fprintf(report,"%d \t %d \t %.3f\n",best_so_far_individual->score, evals, elapsed_time(REAL)); 
	}
	else {
		stall_iters++;
	}
	
	if (iteration - restart_best > restart_iters) {
#ifdef MPI2
		if(auto_coop){
			if (cstall > min_cstall) 
				cstall -= restart_iters * 0.1;
			else cstall = max_cstall;
			if (cstall < min_cstall) 
				cstall = min_cstall;
		}
#endif
		n_restarts++;
		best_so_far_individual->score = individual[iteration_best].score = n;
		stall_iters = 0;
		init_pheromone_trails( trail_0 );
		restart_best = iteration;
	}
}

/*************************    OPT main    *********************************/

void opt_algorithm_aco(JNIEnv* env){

	init_optimization();

	// First iteration (random)
	init_individuals(env);
	update_statistics(env);
	pheromone_trail_update();

	// Next iterations
	while (!termination_condition()) {
		iteration++;
		construct_solutions(env);
		// Daemon actions 
		update_statistics(env);

		#ifdef MPI2
		if(NPROC > 1){
			if (best_so_far_individual->score > optimal)
			   listen_aco(env);
			else
				break;
		}
		#endif
		// Pheromone update
		pheromone_trail_update();
	}   
	exit_optimization();
}

/*************************    MAIN    *********************************/
/*
 This function currently requires a single hdf5 input file that describes the model.
 */

int main(int argc, char **argv) {
	
	// MPI Initialization
#ifdef MPI2    
	init_parallel();
	init_parallel_report();
#else
	init_report();
#endif

	// Parameters can be read from a txt file
	set_default_parameters();
	read_parameters();
	print_parameters();
	
	// Random numbers
#ifdef MPI2
	int seed1 = seed * (mpi_id+1);
	seedR = seedRand(seed1);
#else
	seedR = seedRand(seed);
#endif
	JNIEnv* env = create_vm();
	JavaVM* jvm;
	env->GetJavaVM(&jvm);
	
	for ( ntry = 0 ; ntry < max_tries ; ntry++ ) {
		opt_algorithm_aco(env);
	}

	if (jvm->DestroyJavaVM())
		printf("Failed to destroy Java VM.\n");
	
#ifdef MPI2    
	MPI_Finalize();
#endif
}

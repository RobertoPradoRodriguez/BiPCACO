/**
 * @file individuals.cpp
 * @authors patricia.gonzalez@udc.es / roberto.prado@udc.es
 * @brief File contains procedures related with population
 */
 
#include <assert.h>
#include <limits.h>
#include <time.h>
#include "aco.hpp"

individual_struct *individual;
individual_struct *best_so_far_individual;
individual_struct *best_ever_individual;

int n;
int n_pop;

void allocate_individuals ( void )
/*    
	  FUNCTION:       allocate the memory for the population, the new population and 
					  the best-so-far individual
	  INPUT:          none
	  OUTPUT:         none
*/
{
	int i;
  
	// population
	if((individual = (individual_struct*) malloc(sizeof( individual_struct ) * n_pop +
		sizeof(individual_struct *) * n_pop)) == NULL){
	printf("Out of memory, exit.");
	exit(1);
	}
	for ( i = 0 ; i < n_pop ; i++ ) {
		individual[i].solution = (int*) calloc(n, sizeof(int));
	}

	// best individual
	if((best_so_far_individual = (individual_struct*) malloc(sizeof( individual_struct ) )) == NULL){
		printf("Out of memory, exit.");
		exit(1);
	}
	best_so_far_individual->solution = (int*) calloc(n, sizeof(int));
  
	// best individual EVER
	if((best_ever_individual = (individual_struct*) malloc(sizeof( individual_struct ) )) == NULL){
		printf("Out of memory, exit.");
		exit(1);
	}
	best_ever_individual->solution = (int*) calloc(n, sizeof(int));
}

int find_best( void )
/*    
	  FUNCTION:       find the best individual of the current iteration
	  INPUT:          none
	  OUTPUT:         index of struct containing the iteration best individual
	  (SIDE)EFFECTS:  none
*/
{
	double   min;
	int   k, k_min;

	min = individual[0].score;
	k_min = 0;
	for( k = 1 ; k < n_pop ; k++ ) {
		if( individual[k].score < min ) {
			min = individual[k].score;
			k_min = k;
		}
	}
	return k_min;
}

void copy_from_to(individual_struct *a1, individual_struct *a2)
{
/*    FUNCTION:       copy solution from a1 into a2
	  INPUT:          pointers to the two individuals a1 and a2 
	  OUTPUT:         none
	  (SIDE)EFFECTS:  a2 is copy of a1
*/
	a2->score = a1->score;
	for (int i=0; i < n; i++)
		a2->solution[i] = a1->solution[i];
}
/**
 * @file ants.cpp
 * @authors patricia.gonzalez@udc.es / roberto.prado@udc.es
 * @brief File contains procedures related with ants
 */
 
#include <assert.h>
#include <limits.h>
#include <time.h>
#include "aco.hpp"

double   **pheromone;
double rho;
//double alpha, beta;

int as_flag;
int eas_flag;
int mmas_flag;

double trail_max;
double trail_min;
double trail_0;
int u_gb;

void pheromone_trail_update( void )
/*
 FUNCTION:       manage global pheromone trail update for the ACO algorithms
 INPUT:          none
 OUTPUT:         none
 (SIDE)EFFECTS:  pheromone trails are evaporated and pheromones are deposited
 
 */
{
	// Simulate the pheromone evaporation of all pheromones
	evaporation();
	
	// Apply the pheromone deposit for different ACO variants
	mmas_update();
	
	// Check pheromone trail limits for MMAS
	check_pheromone_trail_limits();
}

void as_update( void )
/*
 FUNCTION:       manage global pheromone deposit for Ant System
 INPUT:          none
 OUTPUT:         none
 (SIDE)EFFECTS:  all ants deposit pheromones on matrix "pheromone"
 */
{
	int   k;
	
	for ( k = 0 ; k < n_pop ; k++ )
		global_update_pheromone( &individual[k] );
}



void eas_update( void )
/*
 FUNCTION:       manage global pheromone deposit for Elitist Ant System
 INPUT:          none
 OUTPUT:         none
 (SIDE)EFFECTS:  best ant so far deposit more pheromone
 */
{
	int   k;
	
	for ( k = 0 ; k < n_pop ; k++ )
		global_update_pheromone( &individual[k] );
	global_update_pheromone_weighted( best_so_far_individual, 2 );
}


void mmas_update( void )
/*
 FUNCTION:       manage global pheromone deposit for MAX-MIN Ant System
 INPUT:          none
 OUTPUT:         none
 (SIDE)EFFECTS:  either the iteration-best or the best-so-far ant deposit pheromone
 on matrix "pheromone"
 */
{
	
	int iteration_best_ant;
	
	if ( iteration % u_gb ) {
		iteration_best_ant = find_best();
		global_update_pheromone(&individual[iteration_best_ant]);
	}
	else {
		global_update_pheromone(best_so_far_individual);
	}
		
	if (( iteration - restart_best ) < (int)(restart_iters/10))
		u_gb = 10;
	else if ((iteration - restart_best) < (int)(restart_iters/5))
		u_gb = 5;
	else if ((iteration - restart_best) < (int)(restart_iters/2))
		u_gb = 3;
	else
		u_gb = 2;
}


void check_pheromone_trail_limits( void )
/*
 FUNCTION:      MMAS keeps pheromone trails inside trail limits
 INPUT:         none
 OUTPUT:        none
 (SIDE)EFFECTS: pheromones are forced to interval [trail_min,trail_max]
 */
{
	int i, j;
	
	for ( i = 0 ; i < n ; i++ ) {
		for ( j = 0 ; j < 2 ; j++ ) {
			if ( pheromone[i][j] < trail_min )
				pheromone[i][j] = trail_min;
			else if ( pheromone[i][j] > trail_max )
				pheromone[i][j] = trail_max;            
		}
	}
}


void init_pheromone_trails( double initial_trail )
/*
 FUNCTION:      initialize pheromone trails
 INPUT:         initial value of pheromone trails "initial_trail"
 OUTPUT:        none
 (SIDE)EFFECTS: pheromone matrix is reinitialized
 */
{
	int i, j;
	
	// Initialize pheromone trails
	for ( i = 0 ; i < n ; i++ ) {
		for ( j =0 ; j < 2 ; j++ ) {
			pheromone[i][j] = initial_trail;
		}
	}
}


void evaporation( void )
/*    
	  FUNCTION:      implements the pheromone trail evaporation
	  INPUT:         none
	  OUTPUT:        none
	  (SIDE)EFFECTS: pheromones are reduced by factor rho
*/
{ 
	int i, j;

	for ( i = 0 ; i < n ; i++ )
		for ( j = 0 ; j < 2 ; j++ )
			pheromone[i][j] = (1 - rho) * pheromone[i][j];
}


void global_update_pheromone( individual_struct *a )
/*    
	  FUNCTION:      reinforces edges used in ant k's solution
	  INPUT:         pointer to ant that updates the pheromone trail 
	  OUTPUT:        none
	  (SIDE)EFFECTS: pheromones of arcs in ant k's tour are increased
*/
{  
	int i, j, h;
	double   d_tau;

	d_tau = 1.0 / a->score;
	for ( i = 0 ; i < n ; i++ ) {
		j = a->solution[i];
		pheromone[i][j] += d_tau;
	}
}


void global_update_pheromone_weighted( individual_struct *a, int weight )
/*    
	  FUNCTION:      reinforces edges of the ant's tour with weight "weight"
	  INPUT:         pointer to ant that updates pheromones and its weight  
	  OUTPUT:        none
	  (SIDE)EFFECTS: pheromones of arcs in the ant's tour are increased
*/
{  
	int i, j;
	double d_tau;

	d_tau = (double) weight / a->score;
	for ( i = 0 ; i < n ; i++ ) {
		j = a->solution[i];
		pheromone[i][j] += d_tau;
	}       
}


void select_gate( individual_struct *a, int gate )
/*    
	  FUNCTION:      chooses for an ant the next gate as the one with
					 maximal value of heuristic information times pheromone 
	  INPUT:         pointer to ant and the construction step
	  OUTPUT:        none 
	  (SIDE)EFFECT:  ant moves to the next gate
*/
{ 
	double prob, prob0, prob1;
	prob  = pheromone[gate][0] + pheromone[gate][1];
	prob0 = pheromone[gate][0] / prob;
	prob1 = pheromone[gate][1] / prob;

	if (genRand( &seedR ) < prob0 )
		a->solution[gate] = 0;
	else
		a->solution[gate] = 1;
}


int distance_between_solutions( int *a1, int *a2)
/*
 FUNCTION: compute the distance between the solutions of ant a1 and a2
 INPUT:    pointers to the two solutions
 OUTPUT:   distance between ant a1 and a2
 */
{
	int i;
	int distance;
	
	distance = 0;
	for ( i = 0 ; i < n ; i++ )
		if (a1[i] != a2[i]) distance++;
	
	return distance;
}


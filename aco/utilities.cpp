/**
 * @file utilities.cpp
 * @authors patricia.gonzalez@udc.es / roberto.prado@udc.es
 * @brief File contains in-out and other misc. procedures
 */

#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include "aco.hpp"

// Limits
int max_tries;
int max_evals;
int max_iters;
double max_time;
int restart_iters;
int stall_iters;

int evals;
int iteration;
int best_iteration;
int restart_best;
int n_restarts;
double best_time;
int optimal;

// Cooperation parameters
#ifdef MPI2
int cfreq;
int cstall;
int auto_coop;
#endif

// W-Model parameters
int _n;
int _mu;
int _nu;
int _gamma;

long int seed;
MTRand seedR;

/* ------------------------------------------------------------------------ */

FILE *report, *final_report, *results_report;

/**************************   TIMER  ***************************************/
static struct rusage res;
static struct timeval tp;
static double virtual_time, real_time;

void start_timers(void)
/*
 FUNCTION:       virtual and real time of day are computed and stored to
 allow at later time the computation of the elapsed time
 (virtual or real)
 INPUT:          none
 OUTPUT:         none
 (SIDE)EFFECTS:  virtual and real time are computed
 */
{
	getrusage( RUSAGE_SELF, &res );
	virtual_time = (double) res.ru_utime.tv_sec +
	(double) res.ru_stime.tv_sec +
	(double) res.ru_utime.tv_usec / 1000000.0 +
	(double) res.ru_stime.tv_usec / 1000000.0;
	
	gettimeofday( &tp, NULL );
	real_time =    (double) tp.tv_sec +
	(double) tp.tv_usec / 1000000.0;
}



double elapsed_time(TIMER_TYPE type)
/*
 FUNCTION:       return the time used in seconds (virtual or real, depending on type)
 INPUT:          TIMER_TYPE (virtual or real time)
 OUTPUT:         seconds since last call to start_timers (virtual or real)
 (SIDE)EFFECTS:  none
 */
{
	if (type == REAL) {
		gettimeofday( &tp, NULL );
		return( (double) tp.tv_sec +
			   (double) tp.tv_usec / 1000000.0
			   - real_time );
	}
	else {
		getrusage( RUSAGE_SELF, &res );
		return( (double) res.ru_utime.tv_sec +
			   (double) res.ru_stime.tv_sec +
			   (double) res.ru_utime.tv_usec / 1000000.0 +
			   (double) res.ru_stime.tv_usec / 1000000.0
			   - virtual_time );
	}
	
}

/**************************   STATISTICS  ***************************************/

/* An implementation of the MT19937 Algorithm for the Mersenne Twister
 * by Evan Sultanik.  Based upon the pseudocode in: M. Matsumoto and
 * T. Nishimura, "Mersenne Twister: A 623-dimensionally
 * equidistributed uniform pseudorandom number generator," ACM
 * Transactions on Modeling and Computer Simulation Vol. 8, No. 1,
 * January pp.3-30 1998.
 *
 * http://www.sultanik.com/Mersenne_twister
 */

#define UPPER_MASK		0x80000000
#define LOWER_MASK		0x7fffffff
#define TEMPERING_MASK_B	0x9d2c5680
#define TEMPERING_MASK_C	0xefc60000


inline static void m_seedRand(MTRand* rand, unsigned long seed) {
	/* set initial seeds to mt[STATE_VECTOR_LENGTH] using the generator
	 * from Line 25 of Table 1 in: Donald Knuth, "The Art of Computer
	 * Programming," Vol. 2 (2nd Ed.) pp.102.
	 */
	rand->mt[0] = seed & 0xffffffff;
	for(rand->index=1; rand->index<STATE_VECTOR_LENGTH; rand->index++) {
		rand->mt[rand->index] = (6069 * rand->mt[rand->index-1]) & 0xffffffff;
	}
}

/**
 * Creates a new random number generator from a given seed.
 */
MTRand seedRand(unsigned long seed) {
	MTRand rand;
	m_seedRand(&rand, seed);
	return rand;
}

/**
 * Generates a pseudo-randomly generated long.
 */
unsigned long genRandLong(MTRand* rand) {
	
	unsigned long y;
	static unsigned long mag[2] = {0x0, 0x9908b0df}; // mag[x] = x * 0x9908b0df for x = 0,1
	if(rand->index >= STATE_VECTOR_LENGTH || rand->index < 0) {
		// generate STATE_VECTOR_LENGTH words at a time
		int kk;
		if(rand->index >= STATE_VECTOR_LENGTH+1 || rand->index < 0) {
			m_seedRand(rand, 4357);
		}
		for(kk=0; kk<STATE_VECTOR_LENGTH-STATE_VECTOR_M; kk++) {
			y = (rand->mt[kk] & UPPER_MASK) | (rand->mt[kk+1] & LOWER_MASK);
			rand->mt[kk] = rand->mt[kk+STATE_VECTOR_M] ^ (y >> 1) ^ mag[y & 0x1];
		}
		for(; kk<STATE_VECTOR_LENGTH-1; kk++) {
			y = (rand->mt[kk] & UPPER_MASK) | (rand->mt[kk+1] & LOWER_MASK);
			rand->mt[kk] = rand->mt[kk+(STATE_VECTOR_M-STATE_VECTOR_LENGTH)] ^ (y >> 1) ^ mag[y & 0x1];
		}
		y = (rand->mt[STATE_VECTOR_LENGTH-1] & UPPER_MASK) | (rand->mt[0] & LOWER_MASK);
		rand->mt[STATE_VECTOR_LENGTH-1] = rand->mt[STATE_VECTOR_M-1] ^ (y >> 1) ^ mag[y & 0x1];
		rand->index = 0;
	}

	y = rand->mt[rand->index++];
	y ^= (y >> 11);
	y ^= (y << 7) & TEMPERING_MASK_B;
	y ^= (y << 15) & TEMPERING_MASK_C;
	y ^= (y >> 18);
	
	return y;
}

/**
 * Generates a pseudo-randomly generated double in the range [0..1].
 */
double genRand(MTRand* rand) {
	return((double)genRandLong(rand) / (unsigned long)0xffffffff);
}


/********  matrix allocation  *******/

int ** generate_int_matrix( int n, int m)
/*    
	  FUNCTION:       malloc a matrix and return pointer to it
	  INPUT:          size of matrix as n x m 
	  OUTPUT:         pointer to matrix
	  (SIDE)EFFECTS:  
*/
{
	int i;
	int **matrix;

	if((matrix = (int**) malloc(sizeof(int) * n * m +
		sizeof(int *) * n	 )) == NULL){
		printf("Out of memory, exit.");
		exit(1);
	}
	for ( i = 0 ; i < n ; i++ )
		matrix[i] = (int *)(matrix + n) + i*m;

	return matrix;
}



double ** generate_double_matrix( int n, int m)
/*    
	  FUNCTION:       malloc a matrix and return pointer to it
	  INPUT:          size of matrix as n x m 
	  OUTPUT:         pointer to matrix
	  (SIDE)EFFECTS:  
*/
{

	int i;
	double **matrix;

	if((matrix = (double**) malloc(sizeof(double) * n * m +
		sizeof(double *) * n	 )) == NULL){
		printf("Out of memory, exit.");
		exit(1);
	}
	for ( i = 0 ; i < n ; i++ )
		matrix[i] = (double *)(matrix + n) + i*m;

	return matrix;
}


/**************************   IN-OUT  ***************************************/

void read_parameters( void )
/*
 FUNCTION:       read input file,
 INPUT:          none
 OUTPUT:         none
 COMMENTS:
 */
{
	char texto[20];
	double numero;

	FILE *params;
	if ((params = fopen("parameters.txt", "r"))==NULL)
		printf("Without parameters file => DEFAULT parameters \n");
	else {
		while (fscanf(params, "%s %lf", texto, &numero) > 1)
		{
			if ( !strcmp(texto,"max_tries") ) max_tries = (int)numero;
			else if( !strcmp(texto,"n_pop") ) n_pop = (int)numero;
			//else if( !strcmp(texto,"alpha") ) alpha = numero;
			//else if( !strcmp(texto,"beta") ) beta = numero;
			else if( !strcmp(texto,"rho") ) rho = numero;
			else if( !strcmp(texto,"restart_iters") ) restart_iters = (int)numero;
			else if( !strcmp(texto,"max_evals") ) max_evals = (int)numero;
			else if( !strcmp(texto,"as_flag") ) as_flag = (int)numero;
			else if( !strcmp(texto,"eas_flag") ) eas_flag = (int)numero;
			else if( !strcmp(texto,"mmas_flag") ) mmas_flag = (int)numero;
			else if( !strcmp(texto,"u_gb") ) u_gb = (int)numero;
			else if( !strcmp(texto,"max_iters") ) max_iters = (int)numero;
			else if( !strcmp(texto,"max_time") ) max_time = numero;
			else if( !strcmp(texto,"optimal") ) optimal = numero;
			else if( !strcmp(texto,"seed") ) seed = (long int)numero;
#ifdef MPI2	
            else if( !strcmp(texto,"cfreq") ) cfreq = (int)numero;
			else if( !strcmp(texto,"cstall") ) cstall = (int)numero;
			else if( !strcmp(texto,"auto_coop") ) auto_coop = (int)numero;
#endif		
            else if( !strcmp(texto,"_n") ) _n = (int)numero;
			else if( !strcmp(texto,"_mu") ) _mu = (int)numero;
			else if( !strcmp(texto,"_nu") ) _nu = (int)numero;
			else if( !strcmp(texto,"_gamma") ) _gamma = (int)numero;	
			else printf(">>>>>>>>> Unknown parameter: %s\n",texto);
		} 
		fclose(params);
	}
}

void init_report( void )
/*
 FUNCTION:       prepare report file,
 INPUT:          none
 OUTPUT:         none
 COMMENTS:
 */
{
	char temp_buffer[255];

	sprintf(temp_buffer,"conv_report");
	report = fopen(temp_buffer, "w");
	sprintf(temp_buffer,"results_report");
	results_report = fopen(temp_buffer, "w");
	sprintf(temp_buffer,"final_report");
	final_report = fopen(temp_buffer, "w");
	fprintf(final_report,"Try:\t Best:\t bIter:\t bTime:\t\t Tot.Time:\t evals:\t\t nrestarts:\t\n");
}


void set_default_parameters(void)
/*
 FUNCTION: set default parameter settings
 INPUT:    none
 OUTPUT:   none
 COMMENTS: none
 */
{
		max_tries	    = 10;
		max_time       	= 100;
		max_iters      	= 10000000;
		max_evals	    = 10000000;
	
		//alpha          	= 1.0;
		//beta           	= 2.0;
		rho            	= 0.5;
		as_flag        	= 0;
		eas_flag       	= 0;
		mmas_flag      	= 1;
		u_gb	   	    = 20;
		restart_iters  	= 100;

		seed           	= (long int) time(NULL);

		optimal         = 0;
		n_pop           = 100;

		_n		= 50;
		_mu		= 2;
		_nu		= 4;
		_gamma	= 0;
#ifdef MPI2
		auto_coop = 0;
		cfreq = 2;
		cstall = 100;
#endif
}

void print_parameters()
/*
 FUNCTION: print parameter settings
 INPUT:    none
 OUTPUT:   none
 COMMENTS: none
 */
{
#ifdef MPI2
	if (mpi_id == 0)
#endif
	{
		printf("\n////////////////////////////////////////////////////\n");
		printf("\nGeneral parameters:\n");
		printf("--------------------------\n");
		printf("max_tries\t\t %d\n", max_tries);
		printf("max_iters\t\t %d\n", max_iters);
		printf("max_evals\t\t %d\n", max_evals);
		printf("max_time (s)\t\t %.2f\n", max_time);
		printf("seed\t\t\t %ld\n", seed);
		printf("optimum\t\t\t %d\n", optimal);
		printf("restart_iters\t\t %d\n", restart_iters);
#ifdef MPI2
		printf("\nCooperation:\n");
		printf("--------------------------\n");
		printf("auto_coop\t\t %d\n", auto_coop);
		if (!auto_coop){
			printf("cfreq\t\t\t %d \t(1 out of %d processes receive)\n", cfreq, cfreq);
			printf("cstall\t\t\t %d\n", cstall);
		}
#endif
		printf("\nACO:\n");
		printf("--------------------------\n");
		printf("n_pop\t\t\t %d\n", n_pop);
		//printf("alpha\t\t\t %.2f\n", alpha);
		//printf("beta\t\t\t %.2f\n", beta);
		printf("rho\t\t\t %.2f\n", rho);
		printf("as_flag\t\t\t %d\n", as_flag);
		printf("eas_flag\t\t %d\n", eas_flag);
		printf("mmas_flag\t\t %d\n", mmas_flag);
		printf("u_gb\t\t\t %d\n", u_gb);
		
		printf("\nW-Model:\n");
		printf("--------------------------\n");
		printf("_n\t\t\t %d\n", _n);
		printf("_mu\t\t\t %d\n", _mu);
		printf("_nu\t\t\t %d\n", _nu);
		printf("_gamma\t\t\t %d\n", _gamma);
		printf("\n////////////////////////////////////////////////////\n\n");
	}
}

void write_report( void )
/*
 FUNCTION:       write report file
 INPUT:          pointer to a solution
 OUTPUT:         none
 */
{
	int i;

	if (final_report){
		fprintf(final_report, "%d\t %d\t %d\t %.3f\t\t %.3f\t\t %d\t\t %d\n",
			ntry, best_ever_individual->score, best_iteration, best_time, elapsed_time(REAL), evals, n_restarts);
	}
	fprintSolutions();
	
	fflush(final_report);
	fflush(report);
	fflush(results_report);

}

void fprintSolutions()
/*
 FUNCTION:       print the solution *t
 INPUT:          pointer to a solution
 OUTPUT:         none
 */
{
	int i,j;
	
	if(results_report) {
		fprintf(results_report,"*********  TRY %d  **********\n",ntry);
			fprintf(results_report,"score: \t %d \t = \t [ ", best_ever_individual->score);
			for( j = 0 ; j < n ; j++ ) {
				fprintf(results_report,"%d ", best_ever_individual->solution[j]);
			}
			fprintf(results_report," ]\n\n");
	}
}
